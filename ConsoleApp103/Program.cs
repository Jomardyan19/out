﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp103
{
    class Program
    {
         static void Main(string[] args)
    {
        int perimetr;   // обьявлонние переменныйе должни совпадат с переменными внутри Math;                        
        int area;   // переменныие в нутри Math и Main не должны(но могут) совпадать друг с другом;

        Math(5, 9, out perimetr, out area);
    }

   static void Math(int x, int y, out int perimetr, out int area)
    {
        perimetr = (x + y) * 2; //обьявлонние переменныйе должни совпадат с переменными внутри Math;
        area = x * y; //переменныие в нутри Math и Main не обезально должны(но могут) совпадать друг с другом;

        Console.WriteLine($"perimetr = {perimetr}");
        Console.WriteLine($"area = {area}");
    }       

    }
}
